const path = require("path");
const fs = require("fs")

const matchesPlayedPerYear = require('./matchesPlayedPerYear.js');
const economy = require('./economy.js');
const  matchesWonPerYear =  require('./matchesWonPerYear.js');
const extraRuns =require('./extraRuns.js')
const csv = require("csvtojson");
const csvFiles1=path.join(__dirname,"../data/matches.csv");
const csvFiles2=path.join(__dirname,"../data/deliveries.csv");
const JSON_OUTPUT_FILE_PATH = "./public/output/data.json";

csv()
 .fromFile(csvFiles1)
 .then((matches)=>{
     csv()
    .fromFile(csvFiles2)
    .then(delivery=>{
    console.log("NUMBER OF MATCHES PLAYED PER YEAR");
    let result1 = matchesPlayedPerYear(matches);
    console.log("NUMBER OF MATCHES WON PER YEAR PER TEAM");
    let result2 =   matchesWonPerYear(matches);
    console.log("EXTRA RUN CONCEDED PER TEAM IN 2016");
    let result3 = extraRuns(matches,delivery);
    console.log("TOP TEN ECONOMICAL BOWLER OF 2015");
    let result4 = economy(matches,delivery);
    //console.log
  //  const jsonString1 = JSON.stringify(jsonData1);
  //  const jsonString2 = JSON.stringify(jsonData2);
  //  const jsonString3 = JSON.stringify(jsonData3);
 //   const jsonString4 = JSON.stringify(jsonData4);
  //  console.log(jsonString1);
  saveData(result1,result2,result3,result4);
    })
    });

   
    function saveData(result1,result2,result3,result4) {
        const jsonData = {
          matchesPlayedPerYear: result1,
          matches_won:result2,
          extra_runs:result3,
          economical_bowler:result4,
        //  winningteam_pervenue:result4
    
        }
        const jsonString = JSON.stringify(jsonData);
        console.log(jsonString)
        fs.writeFileSync(JSON_OUTPUT_FILE_PATH, JSON.stringify(jsonData),(err)=>{
            if(err)
            console.log("Error writing file:", err)
        });
      }
     
   
   