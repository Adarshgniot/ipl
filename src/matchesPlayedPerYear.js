module.exports =  function (csvFiles) {
    const result = {};
    for (let i=0;i<csvFiles.length;i++ ) {
      const season = csvFiles[i].season;
      if (result[season]) {
        result[season] += 1;
      } else {
        result[season] = 1;
      }
    }
    console.log(result);
    return result;
  }
  
  